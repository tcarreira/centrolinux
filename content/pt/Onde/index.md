---
title: "O Centro Linux"
description: "Onde é o Centro Linux"
featured_image: ''
type: page
menu: main
---

# Localização do Centro Linux

O Centro Linux está generosamente alojado no [Makers In Little Lisbon - MILL](https://mill.pt/), pelo qual o Centro Linux está muito grato. Visitem o [sítio web do MILL](https://mill.pt/), para mais informação sobre este Maker Space.

# O MILL

O [Makers In Little Lisbon - MILL](https://mill.pt/), é um [MakerSpace](https://pt.wikipedia.org/wiki/Hackerspace) em Lisboa, situado perto do Campo dos Mártires da Pátria.

## Morada do Centro Linux/MILL:

Calçada do Moinho de Vento, 14B,
1150-236 Lisboa

## Mapa

Localização do MILL no mapa do [OpenStreetMap](https://osm.org/go/b5crq_xVM?layers=N):

[![Localização do MILL](https://centrolinux.pt/images/map.png "mapa com a localização do MILL, clique para mais detalhes e navegação")](https://osm.org/go/b5crq_xVM?layers=N)

## Coordenadas

38.72045,-9.14131?z=19

### Como chegar ao Centro Linux/MILL:
#### Automóvel 
##### Estacionamento

Além do estacionamento na zona, que é gratuito (apenas fim-de-semana) e relativamente abundante no Campo dos Mártires da Pátria, neste local também há um [estacionamento subterrânio](https://osm.org/go/b5eBAVEJs?node=9432160643) págo.

##### Estacionamento subterrâneo

Este estacionamento além de vigiado, está aberto 24h por dia, tem postos de carregamentos para veículso electricos e é possível reservar um lugar por um preço inferior ao máximo diário.
Para preços e outra informação sobre o estacionamento subterrâneo do Campo dos Mártires da Pátria recomendamos a visitar ao [site do operador](https://www.empark.com/pt/pt/parking/lisboa/campo-martires-da-patria/).

Para facilitar a identificação da entrada deixamos uma fotografia da mesma:
![Fotografia da entrada do estacionamento subterrâneo](https://centrolinux.pt/images/Location/photo_2023-02-15_17-24-44.jpg)

[![Mapa com a entrada do estacionamento](https://centrolinux.pt/images/Location/MapaEstacionamento.png "mapa com a localização da entrada do estacionamento subterrânemo do Campo dos Mártires da Pátria. Clique para mais detalhes e navegação")](https://osm.org/go/b5eBAVGDI?node=9432160643)

#### Transportes Públicos

Se utilizar os transportes públicos colectivos, consulte também a secção 'Caminhando', porque os transportes públicos colectivos permitem chegar às imediações do Centro Linux/MILL, mas não à rua onde o mesmo fica localizado. Assim irá necessitar de caminhar algumas dezenas de metros, ou centenas dependendo de qual paragem em que paragem em que sair.

Na zona onde se localiza o Centro Linux/MILL, os transportes públicos colectivos mais próximos são os autocarros da [Carris](https://www.carris.pt/).


Há diversas opções de carreiras utilizando autocarros da Carris que podem deixar os visitantes a uma distância curta do Centro Linux, estas são os que nós identificámos (informação poderá estár desactualizada):
* 19B
* 723
* 760
* 730
* 17B

A Carris oferece no seu site uma [página para planear a vossa viagem](https://www.carris.pt/viaje/planear-viagem/), que também poderá ser útil, e ainda uma página sobre [alterações ao serviço](https://www.carris.pt/viaje/alteracoes-de-servico/) que podem estár em curso durante a vossa utilização do mesmo.


À data da mais recente actualização desta informação nesta nossa página, `a carreira 730 é a única preparada para necessidades de acessibilidade reduzida`. No entanto a Carris mantém uma [página com informação](https://www.carris.pt/viaje/mobilidade-reduzida/) que poderá estár mas actualizada à data desta sua visita ao sítio web do Centro Linux.

O site comunitário e não oficial [intermodal.pt](https://intermodal.pt), também poderá ter informação úti para quem está na Área Metropolitana de Lisboa e se quer deslocar ao Centro Linux/MILL.

##### Mapa de transportes públicos colectivos

![Mapa com informação dos transportes públicos colectivos](https://osm.org/go/b5eBBAlBu--?layers=T&node=4745632624):
[![Mapa com informação dos transportes públicos colectivos](https://centrolinux.pt/images/Location/MapaTP.png "mapa com a localização do MILL e rotas de Transportes Públicos colectivos próximos. Clique para mais detalhes e navegação")](https://osm.org/go/b5eBBAlBu--?layers=T&node=4745632624)


#### Caminhando

##### A partir do Campo dos Mártires da Pátria

Por vários motivos, inclusive por utilização dos transportes públicos colectivos, é provavel que quem se desloque ao Centro Linux/MILL, chegue a partir do Campo dos Mártires da Pátria.

A Calçada do Moinho do Vento, rua em que se encontra localizado o Centro Linux/MILL, junta-se ao Campo dos Mártires da Pátria junto ao seu extremo Súl, próximo da Faculdade de Medicina. Se utilizarem o mapa vão conseguir chegar lá com facilidade.

![Vista da Calçada a partir do Campo dos Mártires da Pátria](https://centrolinux.pt/images/Location/photo_2023-02-15_17-24-49.jpg)

Após chegar ao início da Calçada do Moinho do Vento, é só uma questão de descer a Calçada até encontrar o MILL
Dicas visuais sobre como encontrar a Calçada do Moinho do Vento e o Centro Linux/MILL:

![Entrada da Calçada junto ao Campo dos Mártires da Pátria](https://centrolinux.pt/images/Location/photo_2023-02-15_17-25-01.jpg)


O MILL tem o número de porta 14b:
![O MILL](https://centrolinux.pt/images/Location/photo_2023-02-15_17-25-11.jpg)


#### De Biclicleta

A Calçada do Moinho do Vento tem uma inclinição significativa, pelo que é mais fácil chegar ao Centro Linux/MILL, via Campo dos Mártires da Pátria.

![Mapa com vias cicláveis](https://osm.org/go/b5eBBAIUZ--?layers=Y&way=98032128):
[![Mapa com vias cicláveis](https://centrolinux.pt/images/Location/MapaCiclo.png "mapa com a localização do MILL e vias cicláveis perto. Clique para mais detalhes e navegação")](https://osm.org/go/b5crq_z~8-?layers=C&way=98032128)