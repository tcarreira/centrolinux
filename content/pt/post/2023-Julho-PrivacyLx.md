---
#date: 2023-07-29
description: "Proteger Direitos Humanos e liberdades fundamentais com Tor, Snowflake & Globaleaks"
featured_image: ""
tags: ["2023", "Julho", "Privacidade", "Tor", "Snowflake", "Gobaleaks", "PricacyLx", "Linux"]
title: "Tor, Snowflake & Globaleaks"
---

[![Cartaz](https://centrolinux.pt/images/2023.07-Tor-Snowflake-Globaleaks/cartaz_mill_priv.png)](https://osm.org/go/b5crq_xVM?layers=N)

**Este evento do Centro Linux, é feito em colaboração com a [PrivacyLx](https://privacylx.org/).**

# Tor, Snowflake & Globaleaks

O [Tor](https://www.torproject.org/), [Snowflake](https://snowflake.torproject.org/) e [Globaleaks](https://www.globaleaks.org/) são ferramentas que permitem proteger direitos e liberdades essências em contexto digital, como a privacidade, segurança pessoal, liberdade de expressão, e o capacidade de denunciar e receber denúncias anónimas para proteger fontes jornalísticas (e não só).

Neste evento, vamos aprender com os especialistas da PrivacyLx, como funciona o Tor, e como podemos utilizar o Snowflake para apoiar aqueles que estão em zonas do planeta onde governos opressivos procuram limitar a utilização de Tor para impedir a liberdade expressão e liberdade de imprensa.


## Quando?

No **Sábado dia 29 de Julho de 2023 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

***Mais detalhes brevemente***

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux                                      |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Iníco das actividades                                                        |
| 13:00 | Interrupção para o almoço                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 15:00 | Retomar das actividades no Centro Linux                                      |
| 18:45 | Fim das actividades e desmontagem e arrumação do espaço                      |
| 19:00 | Fim das actividades                                                          |
| ----- | ---------------------------------------------------------------------------- |


### Local:

O Workshop será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições fazem-se **[aqui](https://mill.pt/agenda/tor-snowflake-e-globaleaks/)**, uma pagina no site do Makers in Little Lisbon.

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.
