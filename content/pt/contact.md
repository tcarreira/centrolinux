---
title: Contacto
description: Como nos contactar
featured_image: ''
omit_header_text: false
type: page
menu: main
---

Há várias formas de nos contactar:

* email:
    para assuntos a respeito dos quais não haja um outro endereço especifico pode contactar-nos para o enedereço: geral [ at ] centrolinux ponto pt.
* telegram:
    estamos no grupo de telegram geral da comunidade local de Ubuntu em Portugal, a [Ubuntu-pt](https://t.me/ubuntuptgeral)
* matrix:
    estamos no canal de Matrix da comunidade local de Ubuntu em Portugal, a [Ubuntu-pt](#UbuntuPortugal:matrix.org)

# Social Media:

Estamos presentes em várias redes sociais como:
* o Fediverso ([mastodon](https://pt.wikipedia.org/wiki/Mastodon_(software))) em: [@centrolinux@masto.pt](https://masto.pt/@centrolinux)
* twitter em: [@CentroLinuxPt](https://twitter.com/CentroLinuxPt)
